package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      print(" " * ((10-row)/2))
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c < 0 || c > r) 0
    else if (r == 0) 1
    else pascal(c-1, r-1) + pascal(c, r-1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    @annotation.tailrec
    def _balance(c: List[Char], acc: Int): Boolean = {
      if (c.isEmpty)
        acc == 0
      else {
        if (c.head == '(') _balance(c.tail, acc+1)
        else if (c.head == ')') {
          if (acc > 0) _balance(c.tail, acc-1)
          else false
        } else _balance(c.tail, acc)
      }
    }
    _balance(chars, 0)
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    // http://farhanhubble.wordpress.com/2013/08/16/dissecting-the-count-change-problem/
    if (money == 0) 1
    else if (money < 0 || coins.isEmpty) 0
    else countChange(money, coins.tail) + countChange(money - coins.head, coins)
  }
}
